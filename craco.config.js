const ImageminPlugin = require("imagemin-webpack-plugin").default;

const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");

module.exports = {
  webpack: {
    plugins: [
      new CopyWebpackPlugin(
        {
          patterns: [
            {
              from: "src/images/**/**",
              to: path.resolve(__dirname, "build"),
            },
          ],
        },
      ),
      new ImageminPlugin(),
    ],
  },
};
