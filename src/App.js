import "./App.scss";
import Navbar from "./components/common/navbar/navbar";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Helmet } from "react-helmet";
import Portfolio from "./containers/portfolio/portfolio";
import AboutMe from "./containers/about-me/about-me";
import Footer from "./components/common/footer/footer";

function App() {
  return (
    <BrowserRouter>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Emma Henderson-Ede | Chef</title>
      </Helmet>
      <div className="App">
        <Navbar />
        <Switch>
          <Route exact path="/" component={Portfolio} />
          <Route path="/about-me" component={AboutMe} />
        </Switch>
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
