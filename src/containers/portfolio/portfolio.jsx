import React from "react";
import { Helmet } from "react-helmet";
import Timeline from "../../components/portfolio/timeline";

import "./portfolio.scss";

function Portfolio() {
  return (
    <main className="container" id="portfolio">
      <Helmet>
        <meta charSet="utf-8" />
        <title>Portfolio | Emma Henderson-Ede | Chef</title>
      </Helmet>
      <div id="intro" className="columns is-multiline is-centered">
        <div className="column is-one-third-desktop is-full-mobile">
          <img
            src={"src/images/home-profile.png"}
            alt="A young woman in her chef whites and her hair in a bun getting ready to eat three different breakfast dishes she made at her previous Chef role."
          />
        </div>
        <div id="intro-text" className="column is-one-third-desktop ">
          <h1 className="is-size-1-desktop is-size-2-tablet is-size-3-touch">
            Hi, I’m Emma!
          </h1>
          <p>
            I’m a chef based in Houston, Texas. I’ve spent over 2 years in the
            UK and traveling Europe gaining food knowledge and life experience.
            I moved back to the US in September 2020, and I am currently
            exercising my skills by cooking for those I love around me.
          </p>
        </div>
      </div>
      <Timeline />
    </main>
  );
}

export default Portfolio;
