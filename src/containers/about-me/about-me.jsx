import React from "react";
import { Helmet } from "react-helmet";

import "./about-me.scss";

function AboutMe() {
  return (
    <main id="about-me" className="container">
      <Helmet>
        <title>About Me - Emma Henderson-Ede | Chef</title>
      </Helmet>
          <h1 className="is-size-1-tablet is-size-3-touch">More About Me</h1>
          <div className="columns">
          <div className="column">
          <p>
            Cooking side by side, next to the great women who helped raise me,
            my experience in the kitchen started when I was a mere child.
            Growing up in the deep south of Texas, I was privileged to taste
            some of the best foods this world has to offer. My love for food had
            evolved and as I grew up, I decided to learn how to cook. By
            watching the people at my home, church, and at parties, reading
            cookbooks, and trying new foods to mature my palette, I expanded my
            knowledge of the very thing I loved most: food.
          </p>
          <p>
            I vividly remember my mother and I cooking together, collecting
            every detail she taught me. By the time I was 14, I was able to do
            as much as my mother and more. She allowed me to cook our large
            family dinners most nights, and by that, I was able to gain more
            experience every single day. I regularly served crowds of people.
          </p>
          <p>
            I moved to the UK to be married and live with my now-husband, where
            my large family was then five thousand miles away. I was no longer
            able to cook for them, so in my free time I loved to host dinner
            parties, brunch clubs, and feed the homeless until I landed my first
            restaurant job, with a title of “Chef” next to my name. I worked at
            one of the growing city’s favorite casual pub and restaurants in
            the centre of Bury, Manchester, England, which gave me loads of not
            only culinary experience, but life experience too. I was able to
            finally do what I loved and enjoy what I had created. The comradery
            and relationships created in my first kitchen helped shape the
            person I am today.
          </p>
          <p>
            In September 2020, my husband finished his degree in Manchester, so
            we packed our bags and moved back to Texas to be with my dear
            family. I am now a full-time nanny and tutor, but with a chef’s
            heart. I fill my free time with all things food (and now nutrition,
            due to family members’ nutritional needs). Even though my time is
            currently occupied by children, I have hopes to continue my career
            in the hospitality industry.
          </p>
          <p>
            I am a dynamic, complex person, which is reflected in the many types
            of foods I create. I have been through high highs and low
            lows, giving me the strength to pursue my forever growing passion.
          </p>
          <p>
            Thank you for visiting my portfolio and taking the time to view some
            of my creations. Instead of taking my word for it, I want you to see
            examples of my previous work. I am proud of what I do, although I
            still have a lot to learn.
          </p>
          <p>
            Enjoy,
            <br />
            E. Henderson-Ede
          </p>
        </div>
        <div id="image-container" className="column">
          <img
            src="src/images/profile.jpg"
            alt="A young girl with very long black hair looking into the camera whilst sat on the edge of a floating deck."
          />
        </div>
      </div>
    </main>
  );
}

export default AboutMe;
