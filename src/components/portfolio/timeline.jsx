import React from "react";
import "./timeline.scss";
import LazyLoad from "react-lazyload";

const imageRow1 = [
  {
    image: "src/images/portfolio/12.jpg",
    alt:
      "Braised pork shoulder in a balsamic and garlic tomato sauce, served atop of rustic mashed potatoes, and garnished with parmesan cheese and sliced green onion.",
  },
  {
    image: "src/images/portfolio/11.jpg",
    alt:
      "Smoked salmon fish cake with a poached egg, drizzled with hollandaise, smoked paprika, chives, and a wedge of Meyer lemon. Served with dijon arugula salad.",
  },
  {
    image: "src/images/portfolio/10.jpg",
    alt:
      "Smoked barbecue pulled pork sandwich with pickled red onions and a carrot lemon slaw, on a toasted buttered white bun. Served alongside chips and a dill spear.",
  },
];

const imageRow2 = [
  {
    image: "src/images/portfolio/08.jpg",
    alt:
      "Chinese five spice braised shredded pork, on a lightly toasted French roll. Topped with provolone cheese, caramelized red onion, and garlic mayo arugula salad.",
  },
  {
    image: "src/images/portfolio/18.jpg",
    alt:
      "Blackened tilapia fish tacos made with honey lime coleslaw, pickled red onion, crumbled cotija cheese, garnished with hand picked cilantro and a wedge of lime.",
  },
  {
    image: "src/images/portfolio/03.jpg",
    alt:
      "Pan seared chicken leg quarter, sweet potato puree, Tuscan kale, dollops of whipped goat cheese, drizzled with a pan sauce, and sprinkled with smoked paprika.",
  },
];

const imageRow3 = [
  {
    image: "src/images/portfolio/01.jpg",
    alt:
      "Croque Madame, with ham, Swiss cheese, and dijon mustard, between sliced country bread. Topped with béchamel, a fried egg, sliced scallions, and black pepper.",
  },
  {
    image: "src/images/portfolio/21.jpg",
    alt:
      "A Vietnamese Banh Mi sandwich, assembled with smoked brisket, mayo, quick-pickled daikon and carrots, cucumber batons, thinly sliced red onion, and cilantro.",
  },
  {
    image: "src/images/portfolio/20.jpg",
    alt:
      "Hors d'Oeuvres composed of whipped goat cheese filled phyllo shells, caramelized onions, balsamic reduction, sliced pear, and garnished with English thyme.",
  },
];

const imageRow4 = [
  {
    image: "src/images/portfolio/14.jpg",
    alt:
      "Louisiana-inspired fried catfish dinner including shallow fried mini fillets, classic coleslaw, homemade tartar sauce, and sautéed andouille green beans.",
  },
  {
    image: "src/images/portfolio/13.jpg",
    alt:
      "Nicoise salads built from potato, green bean, red onion, medium-boiled egg, tomato, dark tuna meat, on a bed of lettuce, and dressed with lemon vinaigrette.",
  },
  {
    image: "src/images/portfolio/05.jpg",
    alt:
      "South Korean inspired grilled pork belly. Thick cut pork belly marinated for at least twelve hours in soy sauce, sesame oil, pureed pear, garlic, and wine.",
  },
];

const imageRow5 = [
  {
    image: "src/images/portfolio/02.jpg",
    alt:
      "Hand-folded Chinese wontons (dumplings). Filled with a mixture of ground pork, chopped cabbage, sliced Chinese chives, minced garlic, and pureed ginger.",
  },
  {
    image: "src/images/portfolio/06.jpg",
    alt: "Freshly homemade fettuccine noodles, rustically formed with eggs, olive oil, salt, and all purpose flour. Pressed and cut by a hand-cranked pasta machine.",
  },
  {
    image: "src/images/portfolio/22.jpg",
    alt:
      "Mini profiteroles made from choux pastry, filled with homemade vanilla creme patisserie, topped with a semisweet chocolate ganache.",
  },
];

const imageRow6 = [
  {
    image: "src/images/portfolio/09.jpg",
    alt:
      "Vegetarian “power bowl” consisting of spinach, black beans, mushrooms, fried egg, avocado, and sweet and sticky pan fried tofu, all rested on a bed of rice.",
  },
  {
    image: "src/images/portfolio/23.jpg",
    alt:
      "Classic French onion soup topped with a broiled slice of crusty whole wheat bread, grated gruyere and Swiss cheese, and a sprig of English thyme.",
  },
  {
    image: "src/images/portfolio/19.jpg",
    alt:
      "Traditional American-Italian lasagna, made with a four-hour bolognese sauce, herby ricotta cheese, and lasagna noodles. Garnished with fresh basil chiffonade.",
  },
];

const getImageRow = (imageRow) => (
  <LazyLoad height="750">
    <div className="column is-full">
      <div className="image-row columns">
        {imageRow.map((image, id) => {
          return (
            <div className="image-container column">
              <img key={id} src={image.image} alt={image.alt} />
              <h2 className="image-alt">{image.alt}</h2>
            </div>
          );
        })}
      </div>
    </div>
  </LazyLoad>
);

function Timeline() {
  return (
    <div id="timeline" className="columns is-multiline">
      {getImageRow(imageRow1)}
      {getImageRow(imageRow2)}
      {getImageRow(imageRow3)}
      {getImageRow(imageRow4)}
      {getImageRow(imageRow5)}
      {getImageRow(imageRow6)}
    </div>
  );
}

export default Timeline;
