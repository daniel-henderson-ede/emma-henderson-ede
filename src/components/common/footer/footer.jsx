import React from 'react';
import { FaRegEnvelope, FaLinkedin } from 'react-icons/fa';

import './footer.css';

function Footer() {
  return (
    <footer className="title-container">
      <div className="columns" id="connect">
        <div className="column">
          <h2 className="is-size-4">Connect With Me</h2>
          <p>
            Please don't hesitate to contact me regarding future opportunities or questions pertaining my work.  
          </p>
        </div>
        <div className="column" id="social-media">
          <a
            href="https://www.linkedin.com/in/emmahendersonede/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaLinkedin />
            <span>Connect on LinkedIn</span>
          </a>
          <a href="mailto:emma@hendersonede.com">
            <FaRegEnvelope />
            <span>Email: emma@hendersonede.com</span>
          </a>
        </div>
      </div>
      <div id="footer-text">
        <p>Copyright © 2021 Emma Henderson-Ede. All Rights Reserved.</p>
      </div>
    </footer>
  );
}

export default Footer;
