import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./navbar.scss";

function Navbar() {
  const [isMenuOpen, setMenuOpen] = useState(false);

  function onClick(e) {
    setMenuOpen(!isMenuOpen);
  }

  return (
    <nav
      id="main-nav"
      className="navbar is-fixed-top"
      role="navigation"
      aria-label="main navigation"
    >
      <div className="navbar-brand">
        <Link className="navbar-item" to="/">
          <span className="is-size-4">Emma Henderson-Ede</span>
        </Link>

        <button
          className={`navbar-burger${isMenuOpen ? " is-active" : ""}`}
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarBasicExample"
          onClick={onClick}
        >
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </button>
      </div>

      <div
        id="navbarBasicExample"
        className={`navbar-menu${isMenuOpen ? " is-active" : ""}`}
      >
        <div className="navbar-start">
          <Link to="/" className="navbar-item">
            Portfolio
          </Link>
          <Link to="/about-me" className="navbar-item">
            About Me
          </Link>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
